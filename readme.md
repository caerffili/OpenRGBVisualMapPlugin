# VisualMap 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/commits/master)

## What is this?

This is a plugin for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) that allows you to organize your real devices on a map and create a virtual devices (or many). You can then apply gradients (presets or custom), and expose it to an other plugin (eg. Effect Engine plugin).

## Experimental (Master)

* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Buster 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=Buster%2064)
* [Bullseye 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=Bullseye%2064)
* [Bookworm 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=Bookworm%2064)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## Stable (0.9)

* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/4632289590/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/4632289588/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/4632289591/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/jobs/4632428898/artifacts/download)

You can get older releases [here](https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin/-/releases).

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button

## How do I use it?

Here is a link to find the documentation you need.

[VisualMap Help](https://gitlab.com/OpenRGBDevelopers/OpenRGB-Wiki/-/blob/stable/Plugins/VisualMap/VisualMap.md)
